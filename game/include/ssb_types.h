#ifndef SSB64_TYPES_H
#define SSB64_TYPES_H

#include <PR/ultratypes.h>

typedef u32 uintptr_t;
typedef s32 intptr_t;

#endif /* SSB64_TYPES_H */
