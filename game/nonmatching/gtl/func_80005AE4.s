.section .text
glabel func_80005AE4
  /* 0066E4 80005AE4 27BDFFC0 */     addiu $sp, $sp, -0x40
  /* 0066E8 80005AE8 AFB30024 */        sw $s3, 0x24($sp)
  /* 0066EC 80005AEC 3C138004 */       lui $s3, %hi(D_80045500)
  /* 0066F0 80005AF0 AFB40028 */        sw $s4, 0x28($sp)
  /* 0066F4 80005AF4 AFB20020 */        sw $s2, 0x20($sp)
  /* 0066F8 80005AF8 00809025 */        or $s2, $a0, $zero
  /* 0066FC 80005AFC 27B4003C */     addiu $s4, $sp, 0x3c
  /* 006700 80005B00 26735500 */     addiu $s3, $s3, %lo(D_80045500)
  /* 006704 80005B04 AFBF002C */        sw $ra, 0x2c($sp)
  /* 006708 80005B08 AFB1001C */        sw $s1, 0x1c($sp)
  /* 00670C 80005B0C AFB00018 */        sw $s0, 0x18($sp)
  /* 006710 80005B10 02602025 */        or $a0, $s3, $zero
  /* 006714 80005B14 02802825 */        or $a1, $s4, $zero
  /* 006718 80005B18 0C00C084 */       jal osRecvMesg
  /* 00671C 80005B1C 00003025 */        or $a2, $zero, $zero
  /* 006720 80005B20 2410FFFF */     addiu $s0, $zero, -1
  /* 006724 80005B24 1050000C */       beq $v0, $s0, .L80005B58
  /* 006728 80005B28 3C118004 */       lui $s1, %hi(D_80046638)
  /* 00672C 80005B2C 26316638 */     addiu $s1, $s1, %lo(D_80046638)
  /* 006730 80005B30 8FAE003C */        lw $t6, 0x3c($sp)
  .L80005B34:
  /* 006734 80005B34 02602025 */        or $a0, $s3, $zero
  /* 006738 80005B38 02802825 */        or $a1, $s4, $zero
  /* 00673C 80005B3C 000E7880 */       sll $t7, $t6, 2
  /* 006740 80005B40 022FC021 */      addu $t8, $s1, $t7
  /* 006744 80005B44 00003025 */        or $a2, $zero, $zero
  /* 006748 80005B48 0C00C084 */       jal osRecvMesg
  /* 00674C 80005B4C AF000000 */        sw $zero, ($t8)
  /* 006750 80005B50 5450FFF8 */      bnel $v0, $s0, .L80005B34
  /* 006754 80005B54 8FAE003C */        lw $t6, 0x3c($sp)
  .L80005B58:
  /* 006758 80005B58 3C118004 */       lui $s1, %hi(D_80046638)
  /* 00675C 80005B5C 3C108004 */       lui $s0, %hi(D_80046640)
  /* 006760 80005B60 26316638 */     addiu $s1, $s1, %lo(D_80046638)
  /* 006764 80005B64 26106640 */     addiu $s0, $s0, %lo(D_80046640)
  /* 006768 80005B68 8E020000 */        lw $v0, ($s0) # D_80046640 + 0
  .L80005B6C:
  /* 00676C 80005B6C 3C048004 */       lui $a0, %hi(D_80046638)
  /* 006770 80005B70 24846638 */     addiu $a0, $a0, %lo(D_80046638)
  /* 006774 80005B74 1840000D */      blez $v0, .L80005BAC
  /* 006778 80005B78 00001825 */        or $v1, $zero, $zero
  .L80005B7C:
  /* 00677C 80005B7C 8C990000 */        lw $t9, ($a0) # D_80046638 + 0
  /* 006780 80005B80 17200006 */      bnez $t9, .L80005B9C
  /* 006784 80005B84 3C018004 */       lui $at, %hi(D_80046630)
  /* 006788 80005B88 AC236630 */        sw $v1, %lo(D_80046630)($at)
  /* 00678C 80005B8C 24080001 */     addiu $t0, $zero, 1
  /* 006790 80005B90 AC880000 */        sw $t0, ($a0) # D_80046638 + 0
  /* 006794 80005B94 10000011 */         b .L80005BDC
  /* 006798 80005B98 24020001 */     addiu $v0, $zero, 1
  .L80005B9C:
  /* 00679C 80005B9C 24630001 */     addiu $v1, $v1, 1
  /* 0067A0 80005BA0 0062082A */       slt $at, $v1, $v0
  /* 0067A4 80005BA4 1420FFF5 */      bnez $at, .L80005B7C
  /* 0067A8 80005BA8 24840004 */     addiu $a0, $a0, 4
  .L80005BAC:
  /* 0067AC 80005BAC 16400008 */      bnez $s2, .L80005BD0
  /* 0067B0 80005BB0 02602025 */        or $a0, $s3, $zero
  /* 0067B4 80005BB4 02802825 */        or $a1, $s4, $zero
  /* 0067B8 80005BB8 0C00C084 */       jal osRecvMesg
  /* 0067BC 80005BBC 24060001 */     addiu $a2, $zero, 1
  /* 0067C0 80005BC0 8FA9003C */        lw $t1, 0x3c($sp)
  /* 0067C4 80005BC4 00095080 */       sll $t2, $t1, 2
  /* 0067C8 80005BC8 022A5821 */      addu $t3, $s1, $t2
  /* 0067CC 80005BCC AD600000 */        sw $zero, ($t3)
  .L80005BD0:
  /* 0067D0 80005BD0 5240FFE6 */      beql $s2, $zero, .L80005B6C
  /* 0067D4 80005BD4 8E020000 */        lw $v0, ($s0) # D_80046640 + 0
  /* 0067D8 80005BD8 00001025 */        or $v0, $zero, $zero
  .L80005BDC:
  /* 0067DC 80005BDC 8FBF002C */        lw $ra, 0x2c($sp)
  /* 0067E0 80005BE0 8FB00018 */        lw $s0, 0x18($sp)
  /* 0067E4 80005BE4 8FB1001C */        lw $s1, 0x1c($sp)
  /* 0067E8 80005BE8 8FB20020 */        lw $s2, 0x20($sp)
  /* 0067EC 80005BEC 8FB30024 */        lw $s3, 0x24($sp)
  /* 0067F0 80005BF0 8FB40028 */        lw $s4, 0x28($sp)
  /* 0067F4 80005BF4 03E00008 */        jr $ra
  /* 0067F8 80005BF8 27BD0040 */     addiu $sp, $sp, 0x40

